(function($) {
  'use strict';

  var $buttonBox = $('<div>').css({
    position: 'fixed',
    bottom: '5px',
    right: '5px',
    padding: '5px',
    backgroundColor: '#ffffff',
    border: '#cccccc 1px solid'
  });

  var $button = $('<button>').text('追加ボタン')
  .click(function(e) {
    $(this).parent().prepend('<h1>Hello World</h1>');
  });
  $('body').append($buttonBox.append($button));
})(jQuery);
